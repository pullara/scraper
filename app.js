const axios = require('axios');
const cheerio = require('cheerio');
const WooCommerceApi = require('woocommerce-api');
const yamamotoItems = require('./products/yamamoto.json');
const biotechItems = require('./products/biotech.json');

const wc = new WooCommerceApi({
  url: 'http://bodypowernutrition.local',
  consumerKey: 'ck_48f474076e6427989fb2dbf0b2529e0f6e17c6c9',
  consumerSecret: 'cs_9ac81c2c384e7e7e2805aa2e7575b08fb04ba720',
  wpAPI: true,
  version: 'wc/v2',
  queryStringAuth: true,
});

const importBiotech = (start = 0, end = 0) => {
  biotechItems.slice(start, end).forEach((item) => {
    axios.get(item).then((x) => {
      const $ = cheerio.load(x.data);
      const title = $('.productNameContainer>h1').text();
      const image = $('.productImageSubDiv>img').attr('src');

      const obj = {
        name: title,
        type: 'simple',
        description: $('.productOverviewDiv:first')
          .text()
          .trim(),
        regular_price: 0,
        images: [
          {
            src: `https://it.biotechusa.com${image}`,
            position: 0,
          },
        ],
      };

      wc.post('products', obj, (err) => {
        if (err) throw err;
        console.log(title, 'saved');
      });
    });
  });
};

const importYamamoto = (start = 0, end = 0) => {
  yamamotoItems.slice(start, end).forEach((item) => {
    axios.get(item).then((x) => {
      const $ = cheerio.load(x.data);
      const title = $('h1.prodname').text();
      const image = $('.img-responsive').attr('src');
      const isVariable = $('[data-toggle-name="buttons-taglia"]').length > 0
        && $('[data-toggle-name="buttons-taglia"]>button').length > 1;

      const obj = {
        name: title.replace('®', ''),
        type: isVariable ? 'variable' : 'simple',
        description: $('div[itemprop="description"]')
          .text()
          .trim(),
        regular_price: $('meta[itemprop="price"]').attr('content'),
        images: [
          {
            src: `https://www.yamamotonutrition.com${image.replace('big/b', 'ms/a')}`,
            position: 0,
          },
          {
            src: `https://www.yamamotonutrition.com${image}`,
            position: 1,
          },
        ],
      };

      wc.post('products', obj, (err) => {
        if (err) throw err;
        console.log(title, 'saved');
      });
    });
  });
};

importBiotech(0, 30);
